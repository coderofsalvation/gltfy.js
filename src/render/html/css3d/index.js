// override Object3D

var prot = THREE.Object3D.prototype
var ctor = THREE.Object3D
THREE.Object3D = function Object3D(el,scene){
	if( el ) return new THREE.CSS3DObject(el,scene)
	return ctor.call(this)
}
THREE.Object3D.prototype = prot


GLTFY.on('mode',(mode,m) => {
		
	let scene    = GLTFY.opts.scene
	let renderer = GLTFY.opts.renderer
	let camera   = GLTFY.opts.camera
	if( !scene ) return

	let cleanup = function(){
		console.log("css cleanup")

		scene.traverse(function(obj) {
			if( obj.element ){ 
				// the following actions are needed to reset the dom elements
				// otherwise web2vr fails in translating the (CSStransformed) dom objects to images
				obj.element.style.transform = ''
				obj.element.style.position = '';
				obj.element.style.pointerEvents = '';
				obj.scale.set( 1.0, 1.0, 1.0 )
			}
		})
	}

	let init = () => {
		console.log("renderer/css.js")
		var r = GLTFY.opts.renderer.css
		if( !r ){
			r = GLTFY.opts.renderer.css = new THREE.CSS3DRenderer()
			document.body.appendChild(r.domElement)
			_(renderer)
		}

		renderer.on('setSize', (setSize,w,h) => {
			r.setSize.call(r,w,h)
			setSize.call(renderer,w,h)
		})

		renderer.on('render', (render,scene,camera) => {
			render(scene,camera)
			r.render(scene,camera)
		})
	}

	let nm = mode(m)
	if( m                     ) cleanup()
	if( m && !GLTFY.opts.html ) return
	init()
	GLTFY.update(true)
})

GLTFY.on('update', function(update,everything){
	update(everything)
	if( everything ){
		let scene    = GLTFY.opts.scene
		if( !GLTFY.opts.html ) return
		console.log("updating dom html")
		scene.traverse(function(obj) {
			if( obj.dom && !obj.children.length ){
				var o = new THREE.CSS3DObject( obj.dom )//, scene )
				obj.add(o)
			}
		})
	}
})
