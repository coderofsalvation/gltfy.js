/**
 * Based on http://www.emagix.net/academic/mscs-project/item/camera-sync-with-css3-and-webgl-threejs
 */

var CSS3DObject = THREE.CSS3DObject = function ( element,scene ) {
	THREE.Object3D.call( this );
	var hopts   = element.getAttribute('data-three') 
	var sync_id = element.getAttribute('data-aframe-sync') 
	this.element = element || document.createElement( 'div' );
	this.element.id = this.element.id || "_"+(new Date()).getTime()
	this.element.object3D = this 
	this.element.style.position = 'absolute';
	this.element.style.pointerEvents = 'none';
	var s       = 0.01
	this.scale.set( s, s, s )
	
	var opts  = {}
	if( hopts ) opts = (new Function(`return {${hopts}}`))()
	if( opts.pos ) this.position.set.apply( this.position, opts.pos )

	this.addEventListener( 'removed', function () {

		this.traverse( function ( object ) {

			if ( object.element instanceof Element && object.element.parentNode !== null ) {

				object.element.parentNode.removeChild( object.element );

			}

		} );

	} );
	if( sync_id && _.$(sync_id) ){
		_.$(sync_id).object3D.add( this )
	}
	if( scene ) scene.add(this)

};

CSS3DObject.prototype = Object.assign( Object.create( THREE.Object3D.prototype ), {

	constructor: CSS3DObject,

	copy: function ( source, recursive ) {

		THREE.Object3D.prototype.copy.call( this, source, recursive );

		this.element = source.element.cloneNode( true );

		return this;

	}

} );

var CSS3DSprite = THREE.CSS3DSprite = function ( element ) {

	CSS3DObject.call( this, element );

};

CSS3DSprite.prototype = Object.create( CSS3DObject.prototype );
CSS3DSprite.prototype.constructor = CSS3DSprite;
