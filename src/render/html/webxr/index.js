// aframe fix
AFRAME.THREE.TextGeometry.prototype.__proto__.computeBoundingSphere = THREE.TextGeometry.prototype.computeBoundingSphere

GLTFY.on('mode',(mode,m) => {
		
	let scene    = GLTFY.opts.scene
	let renderer = GLTFY.opts.renderer
	let camera   = GLTFY.opts.camera

	let cleanup = function(){
		console.log("cleanup")
		scene.traverse(function(obj) {
			if( obj.element && obj.web2vr && obj.children.length ){
				obj.children = []
			}
		})
	}


	let XRHTML = function(){
		console.log("initing webxr!")

		this.initCanvas = (obj) => {
			let el             = obj.element
			let selector       = `${obj.element.tagName.toLowerCase()}#${obj.element.id}`
			let canvas 		   = el.sprite.canvas
			console.log("canvas: "+selector)
			let parentSelector = _(obj).get('parent.parentSelector', el.getAttribute("data-aframe-sync") )

			canvas.texture  = new THREE.Texture(el.sprite.canvas);
			canvas.texture.needsUpdate = el.sprite.canvas

			var mat = new THREE.MeshBasicMaterial({
				map: canvas.texture,
				transparent: true,
				opacity:0.5, // *TODO* make configurable
				useScreenCoordinates: false,
				side: THREE.DoubleSide,
				color: 0x00ffff // CHANGED
			});
			//var sp = new THREE.Sprite(mat);
			//sp.scale.set( el.sprite.w, el.sprite.h, 1 ); // CHANGED
			var sp = new THREE.PlaneGeometry( el.sprite.w,el.sprite.h)
			var mesh = new THREE.Mesh(sp,mat) 
			obj.add( mesh );    	
		}

		this.initWeb2VR = (obj) => {
			let selector       = `${obj.element.tagName.toLowerCase()}#${obj.element.id}`
			console.log("web2vr: "+selector)
			let el             = _(obj.element)
			console.dir(el)
			let parentSelector = _(obj).get('parent.parentSelector', el.getAttribute("data-aframe-sync") )
			let opts = {
				parentSelector,
				debug:true,
				position: { x: 0, y: 5.5, z: 0.0 },
				scale: 190,
				skybox:false
			}
			document.body.appendChild(obj.element)
			obj.web2vr = new Web2VR(selector,opts)
			obj.web2vr.start();
			
			/*
			var container = obj.web2vr.aframe.container
			container.object3D.visible = false
			obj.children = container.object3D.children 
			*/
		}
	}

	let nm = mode(m)
	if( m          ) cleanup()
	if( m && m != "webxr" ) return
	GLTFY.xrhtml = new XRHTML()
	GLTFY.update(true)
})

GLTFY.on('update', function(update,everything){
	update(everything)
	if( everything ){
		let scene    = GLTFY.opts.scene
		if( !GLTFY.opts.webxr ) return
		console.log("updating dom web2vr")
		console.log("upadating erverythiiiing")
		scene.traverse(function(obj) {
			if( obj.element && !obj.children.length ){
				if( !obj.web2vr        && !obj.element.sprite ) GLTFY.xrhtml.initWeb2VR(obj)
				if( !obj.canvasTexture &&  obj.element.sprite ) GLTFY.xrhtml.initCanvas(obj)
			}
		})
	}
})
