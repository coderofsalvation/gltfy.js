GLTFY.on('init', function(init){
	init()
	if( AFRAME.components.gltfy ) return
	AFRAME.registerComponent('gltfy', {
		schema:{
			object:    { type:"string" },
			loop:      { type:"number" },
			duration:  { type:"number" },
			animation: { type:"string" }
		},
	    init: function () {
			if( !this.data.object ) return
			let o = GLTFY.create( this.data.object )
			this.el.object3D.add(o)
			// autoplay animation if any
			let opts = {}
			if( this.data.loop      ) opts.loop      = this.data.loop
			if( this.data.duration  ) opts.duration  = this.data.duration
			if( this.data.animation ) o.play( this.data.animation, opts )
		}
	});
})
