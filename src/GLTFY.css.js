GLTFY.exportHTML = function(){

	let cssIds       = ""
	let css          = ""
	let cssKeyframes = ""
	let els    = []
	let custom = {}
	let	js  = `window.GLTFY = window.GLTFY || {}\n`
	js += `GLTFY.on     = GLTFY.on || ${ _.on.toString() }.bind(GLTFY)\n`

	let toCSS = (time, k,v1, v2, v3) => {
		v1 = v1 || 0
		v2 = v2 || 0
		v3 = v3 || 0
		if( k.match(/\.position/) )   return `translate3d( ${v1}em, ${v2}em, ${v3}em );`  
		if( k.match(/\.quaternion/) ) return `rotate3d( ${v1}, ${v2}, ${v3},  3.142rad );`  
		if( k.match(/\.scale/) )      return `scale3d( ${v1}, ${v2}, ${v3} );`  
		return `/*${k}: ${v1}, ${v2}, ${v3} */` 
	}

	let toJS = () => {
		let handler = {
			timeFunc:  (k, v, t )  => js  += `setTimeout( GLTFY.${k.replace(/\(\)/,'')}.bind(GLTFY, '${v}', ${t} ), ${t*1000} )\n`, 
			timePath:  (k, v, t )  => js  += `setTimeout( () => GLTFY['${k}'] = '${v}', ${t*1000} )\n`, 
			path:      (k, v, t )  => js  += `GLTFY['${k}'] = ${ typeof v == 'string' ? `"${v}"` : JSON.stringify(v)}\n`, 
			func:      (k, v, t )  => js  += `GLTFY.${k.replace(/\(\)/, '')}('${v}')\n`, 
			css:       (k, v, t )  => css += `${k} { ${v} }\n`, 
			nop:       (k, v, t )  => ``
		}
		for ( var i in custom ) {
			var isCSS  = i.match(/^\./)
			var isFunc = i.match(/\(\)/) && !isCSS 
			if(  isCSS && i.match(/^\./)  && !isFunc ) handler.css( i, custom[i] )
			if( !isCSS && i.match(/@/  )  && isFunc  ) handler.timeFunc( i.replace(/@.*/, ''),  custom[i],  i.replace(/.*@/, '') )
			if( !isCSS && i.match(/@/  )  && !isFunc ) handler.timePath( i.replace(/@.*/, ''),  custom[i],  i.replace(/.*@/, '') )
			if( !isCSS && !i.match(/@/ )  && !isFunc ) handler.path( i.replace(/@.*/, ''),  custom[i],  i.replace(/.*@/, '') )
			if( !isCSS && !i.match(/@/ )  && isFunc  ) handler.func( i.replace(/@.*/, ''),  custom[i],  i.replace(/.*@/, '') )
		}
	}

	let eachTrack     = (a,i,n,ratio) => (t) =>{
		css += "\t\t"+toCSS(a.tracks[0].times[i], t.name, t.values[i], t.values[i+1], t.values[i+2] )+"\n"
	}
	
	let eachAnimation = (a) => {
		let ratio = 100 / a.duration
		els.push( '#'+a.name.replace(/Action/, '') )
		cssIds += `#${ a.name.replace(/Action/, '') } {\n`
		cssIds += `  animation: ${a.name} ${a.duration}s ease 0s infinite forward;\n`
		cssIds += `}\n`
		cssAnim += `@keyframes ${a.name}{\n`
		for ( var i in a.tracks[0].times  ) {
			cssAnim += "\t"+String( ratio * a.tracks[0].times[i] ).substr(0,4) + "%{\n"
			a.tracks.map( eachTrack(a,i,3,ratio) )
			cssAnim += "\t}\n"
		}
		cssAnim += "}\n"
	}

	let eachModel     = (m) => {
		for ( var i in m.scene.userData  ) 
			custom[ i ] = m.scene.userData[i] 
		m.animations.map( eachAnimation )
	}

	for ( var i in this.models ) eachModel(this.models[i])
	this.models.map( eachModel )
	toJS()

	return {
		js, 
		css:`${cssAnim}\n${cssIds}\n${css}`
	}
}.bind(GLTFY)
