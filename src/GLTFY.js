function GLTFY(){
	_(this) // add util funcs + eventfuncs
	this.models   = []
	this.opts     = {
		mode: "webgl",  // or "html", "html_stereo", "webxr"
		webgl:       true, 
		html:        true,
		html_stereo: false,
		webxr:       false 
	}
	this.parser = new GLTFY.Parser()
	return this
}

/* GLTFY.init()
 *
 * initialize-phase (called during startup)
 *
 * **example**:
 * ```
 * // optional: GLTFY.on('init', (init) => init() )
 * GLTFY.init()
 * ```
 */
GLTFY.prototype.init = function(opts){
	if( opts ) this.opts = Object.assign(this.opts,opts)
	var v;
	let params = (new URL(document.location)).searchParams
	for( var i in this.opts ) 
		if( v = params.get(i) ) this.opts[i] = v
	this.mode( this.opts.mode )	
}

/* GLTFY.play()
 *
 * initialize-phase (called during startup)
 *
 * **example**:
 * ```
 * // optional: GLTFY.on('play', (play) => play() )
 * GLTFY.play()
 * ```
 */
GLTFY.prototype.play = function(url,opts){
	if( opts ) this.opts = Object.assign(this.opts,opts)
	this.init()
	this.load(url)
	return this
}

GLTFY.prototype.stop = function(){ 
	return this
}

GLTFY.prototype.error = function(error){
    var message = (error && error.message) ? error.message : 'Failed to load glTF model';
	console.error(message)
}

GLTFY.prototype.loading = function(a, b, c){

}

GLTFY.prototype.load = function(src){
	this.init()
	this.loading()
}

GLTFY.prototype.loaded = function(src, model){
	model.src = src 
	this.models.push( model )
	if( this.opts.scene && !this.opts.hide ) this.opts.scene.add( model.scene )
	this.update(true)
}

GLTFY.prototype.clone = function(obj, type){
	return Object.assign({}, obj)
}

GLTFY.prototype.create = function(obj_name, scene){
	let obj 
	let anim
	scene = scene || this.opts.scene
	let getAnim   = (m) => m.animations.filter( (a) => a.name == action )[0]// || {tracks:[]}
	let find = () => {
		this.models.map( (m) => {
			m.scene.children.map( (c) => {
				/**/
				if( c.name == obj_name ) obj = this.clone(c, 'mesh', m )
				/*/
				if( c.name == obj_name ){
					obj = this.clone(c, 'mesh', m )
					while(obj.children.length > 0 ) obj.remove(obj.children[0])
					var dummy = new THREE.Object3D();
					c.traverse( (child) => {
						if ( child.isMesh ) {
							var instancedMesh = new THREE.InstancedMesh( child.geometry, child.material, 1 );
							instancedMesh.setMatrixAt( 0, dummy.matrix );
							obj.add( instancedMesh );
						}
					})	
				}
				/**/
			})
		})
	}
	find()
	if( scene ) scene.add( obj )
	return obj
}

GLTFY.prototype.mode = function(m){
	console.log("setting mode to "+m)
	let update = false
	if( m == 'webgl_html'  && (update = true) ) this.opts = Object.assign(this.opts, {webgl:true,  html:true,  html_stereo:false,  webxr:false })
	if( m == 'html'        && (update = true) ) this.opts = Object.assign(this.opts, {webgl:false, html:true,  html_stereo:false,  webxr:false })
	if( m == 'html_stereo' && (update = true) ) this.opts = Object.assign(this.opts, {webgl:false, html:false, html_stereo:true,   webxr:false })
	if( m == 'webxr'       && (update = true) ) this.opts = Object.assign(this.opts, {webgl:true,  html:false, html_stereo:false,  webxr:true  })
	if( update ){
		if( !this.opts.renderer ) return console.error("GLTFY.js: no renderer :/")
		let r = this.opts.renderer 
		if(  r._render ) r.render  = r._render // alway restore original 
		else r._render = r.render              // or backup original
	}
	return this.opts.mode 
}

GLTFY.prototype.update = function(everything){
	if( everything ){
		let r = this.opts.renderer
		let s = r.getSize()
		r.setSize( s.x,  s.y )
	}
}

/* GLTFY.destroy()
 *
 * initialize-phase (called during startup)
 *
 * **example**:
 * ```
 * // optional: GLTFY.on('destroy', (destroy) => destroy() )
 * GLTFY.destroy()
 * ```
 */
GLTFY.prototype.destroy = function(a,b,c){
	console.log("GLTFY inited")
}

GLTFY.prototype.at = function(t){
	return function(fn, v){
		setTimeout( fn.bind(GLTFY, v, t),  t* 1000 )
	}
}

GLTFY.Parser = function(){ 
       // for json-dsl docs see https://www.npmjs.com/package/json-dsl
        var jdsl = (function() {
          return {
            opts: { token: "%s" },
            parseDSL: function(json, data, str, level) {
                var k, nk, nv, v;
                level = level || 0;
                if (str == null) str = '';
                if (typeof json === "object") {
                  for (k in json) {
                        v = json[k];
                        nk = this.parseKey(k,v,level);
                        nv = this.parse(v, data, '', ++level);
                        str += nk.replace(/%s/, nv);
                  }
                }
                if (typeof json === "string") {
                  str += this.parseValue(json, data,level);
                }
                if (str == null) {
                  return json;
                } else {
                  return str;
                }
            },
            parse: function(json, data, str, level) {
                return this.parseDSL(json, data, str, level);
            }
          };
        })();


        jdsl.parseKey = function(k, v, level) {
                if( k.match(/^a-/) ) return "<" + k + " %s></" + k + ">";
                if( level == 1 ) return k + '="%s" '
                return k + ( typeof v == 'boolean' ? '' : ':%s;' )
        }

        jdsl.parseValue = function(v, d, level) {
                return v;
        }

        this.load = (json) => {
                var nodes = json.extensions.GLTF_show.AFRAME.nodes
                return nodes.map( (n) => jdsl.parse(n) ).join("\n")
    }

	return this
}

GLTFY.prototype.log = function(msg){
	console.log("GLTFY.js: "+msg)
}

GLTFY.prototype.audio = function(src){
	console.log("playing "+src)
	new Audio(src).play()
}

console.log(window)
if( typeof window != undefined ) window.GLTFY = new GLTFY()
