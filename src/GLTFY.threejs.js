
GLTFY.on('init', function(init){
	init()
})

GLTFY.play.object = function(action, opts){
	opts = opts || {}
	if( !this.anims[action] ) return GLTFY.log(`animation ${action} not found for mesh ${this.name}`)
	this.action = GLTFY.mixer.clipAction( this.anims[action], this )
	if( opts.duration  ) this.action.setDuration( opts.duration )
	if( !opts.rewind   ) this.action.clampWhenFinished = true
	this.action.play()
	if( opts.loop ) this.action.setLoop( THREE.LoopRepeat, opts.loop )
	return this
}

GLTFY.stop.object = function(){
	if( this.action ) this.action.setLoop( THREE.LoopRepeat, 1 )
	return this
}

GLTFY.on('load', function(load, src){
	load(src)
	this.loader = new THREE.GLTFLoader();
	this.loader.load(src, this.loaded.bind(this, src), this.loading, this.error )
}.bind(GLTFY) )

GLTFY.on('clone', function(clone,obj, type, model){
	var res = type == 'mesh' ? obj.clone() : clone(obj, type)
	let copyAnims = (obj, c, m) => {
		if( !c.anims ){ 
			c.anims = {}
			m.animations.map( (a) => {
				let tracks = a.tracks
				//.filter( (t) => {
				//	return t.name.split('.').slice(0,-1).join('.') == c.name ? true : false
				//})
				if( tracks.length ) c.anims[ a.name ] = Object.assign({}, a, {tracks})
			})
		}
		obj.play       = GLTFY.play.object.bind(obj)
		obj.stop       = GLTFY.stop.object.bind(obj)
		obj.anims      = c.anims
	}
	copyAnims(res, obj, model)
	return res
})

GLTFY.on('loaded', function(loaded, src, model){
	loaded( src, model)
	GLTFY.clock = new THREE.Clock() 
	GLTFY.mixer = new THREE.AnimationMixer(model.scene);
	const clock   = GLTFY.clock
	const mixer   = GLTFY.mixer
	const opts    = GLTFY.opts
	if( opts.animation ){
		var animation = model.animations.filter( (a) => a.name == opts.animation )[0]
		mixer.clipAction(animation).play()
	}
	if( opts.renderer ){
		_( opts.renderer ).on('render', (render,scene, camera) => {
			render( scene, camera )
			mixer.update( clock.getDelta() )
		})
	}
})

GLTFY.on('create',  (create, obj_name, action) => {
	let r = create(obj_name, action)
	return r
})
